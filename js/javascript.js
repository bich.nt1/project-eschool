function mainpage() {
    var windowWidth = jQuery(window).width();
    var widthContainer = $('.container').width();
    var windowHeight = $(window).height();
    var heightHeader = $('header').height();
    var heightFooter = $('footer').height();
    $(".main-page").css("min-height", windowHeight - heightHeader - heightFooter);
}

$(document).ready(function () {
    $('header .navbar-toggle').click(function () {
        $('body').toggleClass('overflow');
    });

    $('.setting-action-edit .setting-action-item').click(function () {
        $(this).parent().hide();
        $('.setting-action-back').css('display','flex');
        $('.setting-item').css('display','flex');
    });

    // $('.datepicker').datepicker();

    $(window).scroll(function () {
        $(window).scrollTop() > 300 ? $(".go_top").addClass("go_tops") : $(".go_top").removeClass("go_tops")
    });

    var swiper = new Swiper(".slide-4-lg .swiper-container", {
        slidesPerView: 4,
        spaceBetween: 60,
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            1200: {
                slidesPerView: 4,
                spaceBetween: 60
            }
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });

    var swiper4 = new Swiper(".slide-4 .swiper-container", {
        slidesPerView: 4,
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            1200: {
                slidesPerView: 4,
                spaceBetween: 20
            }
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });

    mainpage();
    jQuery(window).resize(function () {
        mainpage();
    });

});


